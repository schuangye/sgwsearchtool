﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;

namespace SGWSearchTool
{
    public partial class SetIPForm : Form
    {
        private MainForm m_MainForm = null;
        private ArrayList m_EthNameArry = null;
        private ArrayList m_IPArry = null;
        private ArrayList m_SetIPArgArry = null;

        public SetIPForm(MainForm mainform)
        {
            InitializeComponent();

            m_MainForm = mainform;

            LoadSGWInfor();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (this.IPTextBox.Text == "")
            {
                MessageBox.Show("请输入正确的IP地址!", "提示");
                return;
            }

            string arg = this.NetNameComboBox.Text + ":" +  this.IPTextBox.Text;
            if (this.MaskTextBox.Text != "")
            {
                arg += ":" + this.MaskTextBox.Text;
            }

            if (this.GateIPTextBox.Text != "")
            {
                arg += ":" + this.GateIPTextBox.Text;
            }

            m_SetIPArgArry[this.NetNameComboBox.SelectedIndex] = arg;

            MessageBox.Show("保存成功!", "提示");
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            if (m_SetIPArgArry == null || m_SetIPArgArry.Count == 0)
            {
                MessageBox.Show("设置网关IP地址参数错误!", "提示");
                return;
            }

            //执行设置命令
            ExeSetIPCmd();

            MessageBox.Show("设置成功!", "提示");

            this.Hide();

            m_MainForm.Show();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();  
        }

        private void LoadSGWInfor()
        {
            if (m_EthNameArry == null)
            {
                m_EthNameArry = new ArrayList();
            }

            if (m_IPArry == null)
            {
                m_IPArry = new ArrayList();
            }

            if (m_SetIPArgArry == null)
            {
                m_SetIPArgArry = new ArrayList();
            }

            m_EthNameArry.Clear();
            m_IPArry.Clear();
            m_SetIPArgArry.Clear();

            DataGridView sgwlistdg = (DataGridView)(m_MainForm.Controls.Find("SGWListDataGridView", true)[0]);
            this.SGWNameTextBox.Text = sgwlistdg.SelectedRows[0].Cells[0].Value.ToString();
            this.SGWTypeTextBox.Text = sgwlistdg.SelectedRows[0].Cells[1].Value.ToString();
            this.SGWSNTextBox.Text = sgwlistdg.SelectedRows[0].Cells[2].Value.ToString();
            string ethinfo = sgwlistdg.SelectedRows[0].Cells[3].Value.ToString();

            string[] etharry = ethinfo.Split('|');
            foreach (string eth in etharry)
            {
                string[] tmpetharry = eth.Split(':');
                if (tmpetharry.Length > 1)
                {
                    m_EthNameArry.Add(tmpetharry[0]);
                    m_IPArry.Add(tmpetharry[1]);
                    m_SetIPArgArry.Add(eth);
                }
            }

            this.NetNameComboBox.Items.Clear();
            foreach(string ethname in m_EthNameArry)
            {
                this.NetNameComboBox.Items.Add(ethname);
            }

            if (this.NetNameComboBox.Items.Count > 0)
            {
                this.NetNameComboBox.SelectedIndex = 0;
                this.IPTextBox.Text = m_IPArry[0].ToString();
            }
        }

        private void NetNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] tmpetharry = m_SetIPArgArry[this.NetNameComboBox.SelectedIndex].ToString().Split(':');
            this.IPTextBox.Text = tmpetharry[1];

            this.MaskTextBox.Text = "";
            this.GateIPTextBox.Text = "";
            if (tmpetharry.Length > 2)
            {
                this.MaskTextBox.Text = tmpetharry[2];
            }

            if (tmpetharry.Length > 3)
            {
                this.GateIPTextBox.Text = tmpetharry[3];
            }
        }

        private void SetIPForm_Load(object sender, EventArgs e)
        {
            LoadSGWInfor();
        }

        private void ExeSetIPCmd()
        {
            string setipcmd = m_MainForm.GetSearcheCmd();
            ComboBox localipcombox = (ComboBox)(m_MainForm.Controls.Find("LocalIPComBox", true)[0]);
            TextBox porttxtbox = (TextBox)(m_MainForm.Controls.Find("SearchPortTxtBox", true)[0]);

            string cmdarg = "setipsgw -l " + localipcombox.Text + " -p " + porttxtbox.Text + " -s " + SGWSNTextBox.Text + " ";
            foreach (string ip in m_SetIPArgArry)
            {
                cmdarg = cmdarg + ip + " ";
            }

            //创建进程对象
            Process tmpprocess = new Process();
            tmpprocess.StartInfo.FileName = setipcmd;//设定需要执行的命令  
            tmpprocess.StartInfo.Arguments = cmdarg;
            tmpprocess.StartInfo.UseShellExecute = false;//不使用系统外壳程序启动 
            tmpprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            tmpprocess.StartInfo.RedirectStandardInput = true;//不重定向输入  
            tmpprocess.StartInfo.RedirectStandardOutput = true; //重定向输出  
            tmpprocess.StartInfo.CreateNoWindow = true;//不创建窗口  

            TextBox resulttxtbox = (TextBox)(m_MainForm.Controls.Find("ResultTxtBox", true)[0]);

            //开始进程 
            if (tmpprocess.Start())
            {
                string log = tmpprocess.StandardOutput.ReadLine();
                log = "";
                do
                {
                    log = tmpprocess.StandardOutput.ReadLine();
                    if (log == null)
                    {
                        break;
                    }

                    if (log.Length > 0)
                    {
                        resulttxtbox.AppendText("\r\n" + log + "\r\n");
                    }
                } while (true);

                tmpprocess.Close();
                tmpprocess = null;

                return;
            }

        }
    }
}
