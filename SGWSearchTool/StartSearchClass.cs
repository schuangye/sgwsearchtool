﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Reflection;

namespace SGWSearchTool
{
    class StartSearchClass
    {
        private Process m_curProcess = new Process();

        //声明一个delegate（委托）类型：RefreshDelegate 
        public delegate void RefreshResultDelegate(string log);

        //声明一个delegate（委托）类型：RefreshDelegate 
        public delegate void RefreshStopStatus();

        //声明一个TestDelegate类型的对象。该对象代表了返回值为空，参数只有一个(long型)的方法。它可以搭载N个方法。 
        public RefreshResultDelegate m_RefreshResult;

        //申明更新状态
        public RefreshStopStatus m_RefreshStatus;

        //shell命令
        private string m_SearchCmd = "";

        //shell参数
        private string m_SearchCmdArg = "";

        //启动线程
        public void StartThead(string cmd, string cmdarg)
        {
            if (cmd.Length == 0 || cmdarg.Length == 0)
            {
                return;
            }

            m_SearchCmd     = cmd;
            m_SearchCmdArg  = cmdarg;

            Thread startThread = new Thread(StartSearch);
            startThread.Start();

            return;
        }

        //停止线程
        public void StopThead()
        {
            try
            {
                m_curProcess.WaitForExit();
            }
            catch (System.Exception ex)
            {

            }
        }


        /// 开始搜索 
        /// <summary>          
        /// </summary> 
        public void StartSearch()
        {
            //创建进程对象
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = m_SearchCmd;//设定需要执行的命令  
            startInfo.Arguments = m_SearchCmdArg;
            startInfo.UseShellExecute = false;//不使用系统外壳程序启动 
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.RedirectStandardInput = true;//不重定向输入  
            startInfo.RedirectStandardOutput = true; //重定向输出  
            startInfo.CreateNoWindow = true;//不创建窗口  
            m_curProcess.StartInfo = startInfo;

            //开始进程 
            if (m_curProcess.Start())
            {
                m_curProcess.StandardInput.WriteLine("e\n");
                m_curProcess.StandardOutput.ReadLine().Trim();

                //表示已经搜索完成，停止定时器
                m_RefreshStatus();

                string log = m_curProcess.StandardOutput.ReadLine();
                m_RefreshResult(log);
                log = "";
                do
                {
                    log = m_curProcess.StandardOutput.ReadLine();
                    if (log == null)
                    {
                        break;
                    }

                    if (log.Length > 0)
                    {
                        m_RefreshResult(log);
                    }

                    if (log.IndexOf("Search smart gateway success") > -1)
                    {
                        m_RefreshResult("Search Finish");
                        break;
                    }
                } while (true);

                m_curProcess.Close();
                m_curProcess = null;

                return;
            }

            return;
        }
    }
}
