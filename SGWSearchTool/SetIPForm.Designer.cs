﻿namespace SGWSearchTool
{
    partial class SetIPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SGWSNTextBox = new System.Windows.Forms.TextBox();
            this.SGWTypeTextBox = new System.Windows.Forms.TextBox();
            this.SGWNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.NetNameComboBox = new System.Windows.Forms.ComboBox();
            this.GateIPTextBox = new System.Windows.Forms.TextBox();
            this.MaskTextBox = new System.Windows.Forms.TextBox();
            this.IPTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SGWSNTextBox);
            this.groupBox1.Controls.Add(this.SGWTypeTextBox);
            this.groupBox1.Controls.Add(this.SGWNameTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 141);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "网关基本信息";
            // 
            // SGWSNTextBox
            // 
            this.SGWSNTextBox.Location = new System.Drawing.Point(78, 104);
            this.SGWSNTextBox.Name = "SGWSNTextBox";
            this.SGWSNTextBox.ReadOnly = true;
            this.SGWSNTextBox.Size = new System.Drawing.Size(265, 21);
            this.SGWSNTextBox.TabIndex = 5;
            // 
            // SGWTypeTextBox
            // 
            this.SGWTypeTextBox.Location = new System.Drawing.Point(78, 67);
            this.SGWTypeTextBox.Name = "SGWTypeTextBox";
            this.SGWTypeTextBox.ReadOnly = true;
            this.SGWTypeTextBox.Size = new System.Drawing.Size(265, 21);
            this.SGWTypeTextBox.TabIndex = 4;
            // 
            // SGWNameTextBox
            // 
            this.SGWNameTextBox.Location = new System.Drawing.Point(78, 33);
            this.SGWNameTextBox.Name = "SGWNameTextBox";
            this.SGWNameTextBox.ReadOnly = true;
            this.SGWNameTextBox.Size = new System.Drawing.Size(265, 21);
            this.SGWNameTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "网关编码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "网关类型";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "网关名称";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SaveButton);
            this.groupBox2.Controls.Add(this.NetNameComboBox);
            this.groupBox2.Controls.Add(this.GateIPTextBox);
            this.groupBox2.Controls.Add(this.MaskTextBox);
            this.groupBox2.Controls.Add(this.IPTextBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(13, 188);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 163);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "IP地址信息";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(205, 122);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(72, 23);
            this.SaveButton.TabIndex = 8;
            this.SaveButton.Text = "保存";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // NetNameComboBox
            // 
            this.NetNameComboBox.FormattingEnabled = true;
            this.NetNameComboBox.Location = new System.Drawing.Point(76, 24);
            this.NetNameComboBox.Name = "NetNameComboBox";
            this.NetNameComboBox.Size = new System.Drawing.Size(111, 20);
            this.NetNameComboBox.TabIndex = 7;
            this.NetNameComboBox.SelectedIndexChanged += new System.EventHandler(this.NetNameComboBox_SelectedIndexChanged);
            // 
            // GateIPTextBox
            // 
            this.GateIPTextBox.Location = new System.Drawing.Point(76, 123);
            this.GateIPTextBox.Name = "GateIPTextBox";
            this.GateIPTextBox.Size = new System.Drawing.Size(111, 21);
            this.GateIPTextBox.TabIndex = 6;
            // 
            // MaskTextBox
            // 
            this.MaskTextBox.Location = new System.Drawing.Point(76, 91);
            this.MaskTextBox.Name = "MaskTextBox";
            this.MaskTextBox.Size = new System.Drawing.Size(111, 21);
            this.MaskTextBox.TabIndex = 5;
            this.MaskTextBox.Text = "255.255.255.0";
            // 
            // IPTextBox
            // 
            this.IPTextBox.Location = new System.Drawing.Point(76, 60);
            this.IPTextBox.Name = "IPTextBox";
            this.IPTextBox.Size = new System.Drawing.Size(111, 21);
            this.IPTextBox.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "网关";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "子网掩码";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "IP地址";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "网卡名";
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(309, 220);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 2;
            this.ApplyButton.Text = "应用";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(309, 282);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "取消";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // SetIPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 363);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetIPForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "设置网关IP";
            this.Load += new System.EventHandler(this.SetIPForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SGWNameTextBox;
        private System.Windows.Forms.TextBox SGWSNTextBox;
        private System.Windows.Forms.TextBox SGWTypeTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox IPTextBox;
        private System.Windows.Forms.TextBox GateIPTextBox;
        private System.Windows.Forms.TextBox MaskTextBox;
        private System.Windows.Forms.ComboBox NetNameComboBox;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SaveButton;

    }
}