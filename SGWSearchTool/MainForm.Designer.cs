﻿using System.Windows.Forms;
namespace SGWSearchTool
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TimesTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btStop = new System.Windows.Forms.Button();
            this.btsearch = new System.Windows.Forms.Button();
            this.SearchPortTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LocalIPComBox = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ResultTxtBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SGWListDataGridView = new System.Windows.Forms.DataGridView();
            this.SGWName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SGWListDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TimesTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btStop);
            this.groupBox1.Controls.Add(this.btsearch);
            this.groupBox1.Controls.Add(this.SearchPortTxtBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.LocalIPComBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(788, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "搜索参数";
            // 
            // TimesTextBox
            // 
            this.TimesTextBox.Location = new System.Drawing.Point(444, 30);
            this.TimesTextBox.MaxLength = 6;
            this.TimesTextBox.Name = "TimesTextBox";
            this.TimesTextBox.Size = new System.Drawing.Size(68, 21);
            this.TimesTextBox.TabIndex = 7;
            this.TimesTextBox.Text = "1";
            this.TimesTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TimesTextBox_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(379, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "搜索次数：";
            // 
            // btStop
            // 
            this.btStop.Enabled = false;
            this.btStop.Location = new System.Drawing.Point(670, 30);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(75, 21);
            this.btStop.TabIndex = 5;
            this.btStop.Text = "停止";
            this.btStop.UseVisualStyleBackColor = true;
            this.btStop.Click += new System.EventHandler(this.btStop_Click);
            // 
            // btsearch
            // 
            this.btsearch.Location = new System.Drawing.Point(572, 28);
            this.btsearch.Name = "btsearch";
            this.btsearch.Size = new System.Drawing.Size(75, 23);
            this.btsearch.TabIndex = 4;
            this.btsearch.Text = "开始";
            this.btsearch.UseVisualStyleBackColor = true;
            this.btsearch.Click += new System.EventHandler(this.btsearch_Click);
            // 
            // SearchPortTxtBox
            // 
            this.SearchPortTxtBox.Location = new System.Drawing.Point(305, 29);
            this.SearchPortTxtBox.MaxLength = 6;
            this.SearchPortTxtBox.Name = "SearchPortTxtBox";
            this.SearchPortTxtBox.Size = new System.Drawing.Size(68, 21);
            this.SearchPortTxtBox.TabIndex = 3;
            this.SearchPortTxtBox.Text = "9000";
            this.SearchPortTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchPortTxtBox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(240, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "搜索端口：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "本地IP：";
            // 
            // LocalIPComBox
            // 
            this.LocalIPComBox.FormattingEnabled = true;
            this.LocalIPComBox.Location = new System.Drawing.Point(64, 28);
            this.LocalIPComBox.Name = "LocalIPComBox";
            this.LocalIPComBox.Size = new System.Drawing.Size(160, 20);
            this.LocalIPComBox.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ResultTxtBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 289);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(788, 206);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "搜索日志";
            // 
            // ResultTxtBox
            // 
            this.ResultTxtBox.BackColor = System.Drawing.Color.Black;
            this.ResultTxtBox.ForeColor = System.Drawing.Color.Lime;
            this.ResultTxtBox.Location = new System.Drawing.Point(9, 21);
            this.ResultTxtBox.Multiline = true;
            this.ResultTxtBox.Name = "ResultTxtBox";
            this.ResultTxtBox.ReadOnly = true;
            this.ResultTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ResultTxtBox.Size = new System.Drawing.Size(767, 173);
            this.ResultTxtBox.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SGWListDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(12, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(788, 176);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "网关列表";
            // 
            // SGWListDataGridView
            // 
            this.SGWListDataGridView.AllowUserToAddRows = false;
            this.SGWListDataGridView.AllowUserToDeleteRows = false;
            this.SGWListDataGridView.BackgroundColor = System.Drawing.Color.Silver;
            this.SGWListDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SGWListDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.SGWListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SGWListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SGWName,
            this.Type,
            this.SN,
            this.NetWork});
            this.SGWListDataGridView.EnableHeadersVisualStyles = false;
            this.SGWListDataGridView.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.SGWListDataGridView.Location = new System.Drawing.Point(14, 20);
            this.SGWListDataGridView.MultiSelect = false;
            this.SGWListDataGridView.Name = "SGWListDataGridView";
            this.SGWListDataGridView.RowTemplate.Height = 23;
            this.SGWListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SGWListDataGridView.Size = new System.Drawing.Size(762, 148);
            this.SGWListDataGridView.TabIndex = 5;
            this.SGWListDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            // 
            // SGWName
            // 
            this.SGWName.HeaderText = "网关名称";
            this.SGWName.Name = "SGWName";
            this.SGWName.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.HeaderText = "网关类型";
            this.Type.Name = "Type";
            this.Type.Width = 120;
            // 
            // SN
            // 
            this.SN.HeaderText = "SN";
            this.SN.Name = "SN";
            this.SN.ReadOnly = true;
            this.SN.Width = 170;
            // 
            // NetWork
            // 
            this.NetWork.HeaderText = "网络 ";
            this.NetWork.Name = "NetWork";
            this.NetWork.ReadOnly = true;
            this.NetWork.Width = 260;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 504);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "网关搜索工具V1.2";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SGWListDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox LocalIPComBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SearchPortTxtBox;
        private System.Windows.Forms.Button btsearch;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox TimesTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btStop;
        private GroupBox groupBox2;
        private TextBox ResultTxtBox;
        private GroupBox groupBox3;
        private DataGridView SGWListDataGridView;
        private DataGridViewTextBoxColumn SGWName;
        private DataGridViewTextBoxColumn Type;
        private DataGridViewTextBoxColumn SN;
        private DataGridViewTextBoxColumn NetWork;
    }
}

