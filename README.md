# SGWSearchTool

#### 介绍
工具可以搜索在同一个局域网下的网络设备，支持搜索次数，使用网网卡，以及搜索端口选择配置。如果网络设备需要支持，可以通过集成代理方式直接支持。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/134751_a99cefd4_305663.png "屏幕截图.png")

#### 修改记录
2020-12-16 增加修改设备IP功能

![输入图片说明](https://images.gitee.com/uploads/images/2020/1216/093631_0a2d3ea9_305663.png "屏幕截图.png")

#### 技术支持
    QQ:93911329 邮箱：93921329@qq.com

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/151139_d938ac4c_305663.jpeg "contact.jpg")
